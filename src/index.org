#+TITLE:  Index to Artefacts and Realization Catalog
#+AUTHOR: VLEAD
#+DATE: [2018-12-05 Wed]
#+SETUPFILE: ../org-templates/level-1.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introductions:
This is an index to implementation of artefacts and
realization catalog.

* References
A mapping between different implementations and their roles.

|------+-------------------------------------+------------------------------------------------------------|
| S.No | Artefacts and Realization Catalog   | Role                                                       |
|------+-------------------------------------+------------------------------------------------------------|
|    1 | [[./runtime/html/mst_kruskal_demo.org][kruskals_algo-demo]]         | Holds the demo for Kruskals Algorithm     |
|------+-------------------------------------+------------------------------------------------------------|
|    2 | [[./runtime/html/mst_kruskal_exercise.org][kruskals_algo-exercise]]| Holds the exercise for Kruskals Algorithm  |
|------+-------------------------------------+------------------------------------------------------------|
|    3 | [[./runtime/html/mst_kruskal_practice.org][kruskas_algo-practice]]| Holds the practice artefact for Kruskals Algorithm|
|------+-------------------------------------+------------------------------------------------------------|
|    4 | [[./runtime/html/mst_prims_demo.org][prims_algo-demo]]| Holds the demo for optimised Kruskals Algorithm|
|------+-------------------------------------+------------------------------------------------------------|
|    5 | [[./runtime/html/mst_prims_exercise.org][prims_algo-exercise]]| Holds the exercise for Kruskals Algorithm|
|------+-------------------------------------+------------------------------------------------------------|
|    6 | [[./runtime/html/mst_prims_practice.org][prims_algo-practice]]| Holds the practice artefact for Kruskals Algorithm|
|------+-------------------------------------+------------------------------------------------------------|
|    8 | [[./runtime/realization-catalog.org][realization catalog]]| Holds the realization catalog|
|------+-------------------------------------+------------------------------------------------------------|
