#+TITLE:
#+AUTHOR:VLEAD
#+DATE:
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
  This document builds the =Kruskal's demo= interactive
  artefact(HTML).

* Features of the artefact
+ Artefact provides a demo module for Kruskal's Algorithm
+ User can see a randomly generate graph or work on same
  graph as many times as he need.
+ User can click the =New Graph= button to work on a new graph at any time.
+ User can click the =Reset= button to work on same graph at any time.

* HTML Framework of Kruskal's Demo
** Head Section Elements
Title, meta tags and all the links related to Js, CSS of
this artefact comes under this section.
#+NAME: head-section-elems
#+BEGIN_SRC html
<title>Kruskal's</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script src="./../../../docs/static/libs/vis.min.js"></script>
<script src="./../js/Algo.js"></script> 
<script src="./../js/data.js"></script> 
<link href="./../../../docs/static/libs/vis.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://ds1-iiith.vlabs.ac.in/exp-common-css/common-styles.css">
<link href="./../css/mst.css" rel="stylesheet" type="text/css" />
<script src="./../../../docs/static/libs/vis.anim.js"></script>
<script src="https://ds1-iiith.vlabs.ac.in/exp-common-js/instruction-box.js"></script>
<script src="./../js/mst_kruskal_demo.js"></script> 

#+END_SRC

** Body Section Elements
*** Instruction box
This is an instruction box, in which the instructions needed
for the user to perform the experiments are displayed in a
form of list.
#+NAME: instruction-box
#+BEGIN_SRC html
<div class="instruction-box">
  <button class="collapsible"> Instructions </button>
  <div class="content">
    <li>Zoom or Adjust graph by dragging nodes according to your convience</li>  
    <li>Click on <b>Start</b> to start demo with given graph</li>
    <li>After clicking Start click on <b>Next</b> to move a step forward</li>
    <li>You can click on <b>Pause</b> or <b>Play</b> to
    pause and play the demo respectively and can adjust the
    speed using Speed Adjust Slider
    </li>
    <li>Click on <b>New Graph</b> to start over with a new graph</li>
    <li>Click on <b>Reset</b> to work again on same graph</li>
  </div>
</div>

#+END_SRC
 
*** Legend box
This is a legend box, that can referred to when observing.
#+NAME: legend-box
#+BEGIN_SRC html
<div id="legend" >
  <ul class="legend">
    <li><span class="blac"></span> Not Visited</li>
    <li><span class="brow"></span> Edge added to MST</li>
    <li><span class="grey"></span> Edge discarded</li>
    <li><span class="blu"></span> Edge being checked</li>
  </ul>
</div>

#+END_SRC

*** Graph Holder
This is a graph holder, which contains graph in canvas where
the graphs are randomly created.
#+NAME: graph-holder
#+BEGIN_SRC html
  <div id="graph"></div> 

#+END_SRC

*** Slider
This is a slider to which =change_interval= method is
appended to control the demonstration speed of an artefact.
#+NAME: slider
#+BEGIN_SRC html
  <div>
    <p id="ranger">
      Min.Speed
      <input type="range" id="myRange" class="slider">
      Max.Speed
    </p>
  </div>

#+END_SRC

*** Controller Elements
This are the controller buttons(Start, Reset, Pause) of an
artefacts used for performing/demonstrating the artefact.
#+NAME: controller-elems
#+BEGIN_SRC html
<div class="buttons">
  <button id="startnext"  class="button-input">Start</button>
  <button id="rege" class="button-input">New Graph</button>
  <button id="res" class="button-input">Reset</button>
  <button id="pause" class="button-input">Pause</button>  
</div>

#+END_SRC

*** Comments Box
This is a box in which the comments gets displayed based on
what is happening currently in the artefact.
#+NAME: comments-box
#+BEGIN_SRC html
<div id="instruction" class="comment-box">
  <b>Observations</b><br>
  <p id="ins"> </p>
</div>

#+END_SRC

* Tangle
#+BEGIN_SRC html :tangle mst_kruskal_demo.html :eval no :noweb yes
<!DOCTYPE HTML>
<html lang="en">
  <head>
    </head>
    <body>
    <<instruction-box>>
    <<graph-holder>>
    <<legend-box>>
    <<slider>>
    <<comments-box>>  
    <<controller-elems>>
    <<head-section-elems>>
  </body>
</html>
#+END_SRC
