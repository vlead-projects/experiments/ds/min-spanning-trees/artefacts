#+TITLE:
#+AUTHOR:VLEAD
#+DATE:
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
  This document builds the =Prim's exercise= interactive
  artefact(HTML).

* Features of the artefact
+ Artefact provides a practice module for Prim's Algorithm
+ User can see a randomly generate graph or work on same
  graph as many times as he need.
+ User can click the =New Graph= button to work on a new
  graph at any time.
+ User can click the =Reset= button to work on same graph at
  any time.
+ User has two options at each step clciking on a previous
  selected edge and clciking on a new edge .
+ Clicks on edge for second time if its to be added to be  MST.
+ Clicks on new edge for first time if its to checked next.
+ You receive "Correct" and "Incorrect" accordingly

* HTML Framework of Prim's Exercise
** Head Section Elements
Title, meta tags and all the links related to Js, CSS of
this artefact comes under this section.
#+NAME: head-section-elems
#+BEGIN_SRC html
<title>Prim's</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script src="./../../../docs/static/libs/vis.min.js"></script>
<script src="./../js/Algo2.js"></script> 
<script src="./../js/data.js"></script> 
<link href="./../../../docs/static/libs/vis.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://ds1-iiith.vlabs.ac.in/exp-common-css/common-styles.css">
<link href="./../css/mst.css" rel="stylesheet" type="text/css" />
<script src="https://ds1-iiith.vlabs.ac.in/exp-common-js/instruction-box.js"></script>
<script src="./../js/mst_prims_exercise.js"></script> 
#+END_SRC

** Body Section Elements
*** Instruction box
This is an instruction box, in which the instructions needed
for the user to perform the experiments are displayed in a
form of list.
#+NAME: instruction-box
#+BEGIN_SRC html
<div class="instruction-box">
  <button class="collapsible">Instructions</button>
  <div class ="content">
    <ul>
      <li>Select the node you want to start prims from.</li>
      <li>Select the node you want to start Prims from.</li>
      <li>Select the edge to be checked.</li>
      <li>Click on the selected edge if it is to be added .</li> 
      <li>Click on <b>Reset</b> to start over with the same graph</li>
      <li>Click on <b>New Graph</b> to start over with a new graph</li>
      <li>Zoom or Adjust graph by dragging nodes according to your convenience</li> 
    </ul> 
  </div>
</div>

#+END_SRC
 
*** Question
This is question user should answer.
#+NAME: question-prims
#+BEGIN_SRC html
<div id="que">
  <b>Question :</b>Maximum spanning tree is the spanning tree with maximum length of tree, apply the concept of Prims to find Maximum spanning tree of graph below
  {<b>Hint :</b> Sort and check in decreasing order instead of increasing order which was the case in finding Min Spanning tree and follow the Prims Algo}
</div>
#+END_SRC
 
*** Legend box
This is a legend box, that can referred to when observing.
#+NAME: legend-box
#+BEGIN_SRC html
<div id="legend" >
  <ul class="legend">
    <li><span class="blac"></span> Not Visited</li>
    <li><span class="lim"></span> Edges to be checked</li>
    <li><span class="brow"></span> Added to MST</li>
    <li><span class="vio"></span> Edge being checked</li>
  </ul>
</div>
#+END_SRC

*** Graph Holder
This is a graph holder, which contains graph in canvas where
the graphs are randomly created
#+NAME: graph-holder
#+BEGIN_SRC html
<center>   
  <div id="graph"></div> 
</center>

#+END_SRC

*** COMMENT Comments Box
This is a box in which the comments gets displayed based on
what is happening currently in the artefact.
#+NAME: comments-box
#+BEGIN_SRC html
<div id="instruction"class="comment-box">
  <b>Observations</b><br>
  <p id = "ins"> </p>
</div>

#+END_SRC

*** Controller Elements
This are the controller buttons(Start, Reset, Pause) of an
artefacts used for performing/demonstrating the artefact.
#+NAME: controller-elems
#+BEGIN_SRC html
<div class="buttons">
  <button id="res" onclick="reset()" class="button-input">Reset</button>
  <button id="rege" onclick="regen()" class="button-input">New Graph</button>
  <button id="subm" onclick="submit()" class="button-input">Submit</button>
</div>

#+END_SRC

* Tangle
#+BEGIN_SRC html :tangle mst_prims_exercise.html :eval no :noweb yes
<!DOCTYPE HTML>
<html lang="en">
  <head>
  </head>
  <body>
    <<instruction-box>> 
    <<question-prims>>
    <<graph-holder>>
    <<legend-box>>
    <<comments-box>>
    <<controller-elems>>
    <<head-section-elems>>
  </body>
</html>
#+END_SRC
